Supplementary materials of

[1] Construction and convergence analysis of conservative second order 
    local time discretisation for wave equations based on domain decomposition. 
    Juliette Chabassier and S?bastien Imperiale


The provided files (listed below) can be used to recover all the numerical results presented in Sections 4.3, 5 and 6 of [1].
The files are .m files that were run in [1] with Matlab R2016b.


List of files: 

		script_section_X.m: Run several times solver.m with the adequate value of parameters in order to obtain all the convergence plots of Section X of [1].

		solver.m: Solve the 1D wave equation on an interval for a given input list of parameters. The outputs are the relative errors with the exact solution. 
		
		fun_r.m,fun_g.m,fun_h.m: Implementation of the functions r,g,h as defined in Section 5 and 6 of [1] that are necessary to compute initial conditions and/or source terms.
        
        Optimize_polynoms.m : Computes the values (a,b,beta_epsilon) for the construction on polynomials in Section 4.3 of [1]

        RunOptimization.m : Calls Optimize_polynoms for adequate values (different \ell and epsilon), used to fill Table 1 of [1] 






