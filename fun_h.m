% Supplementary materials of
%
% [1] Construction and convergence analysis of conservative second order 
%     local time discretisation for wave equations based on domain decomposition. 
%     Juliette Chabassier and Sebastien Imperiale
%
% __author__ = "Sebastien Imperiale"
% __copyright__ = "Copyright 2019, Inria"
% __credits__ = ["Sebastien Imperiale"]
% __license__ = "GPL 3.0"
% __version__ = "1.0"
% __maintainer__ = "S?bastien Imperiale"
% __email__ = "sebastien.imperiale@inria.fr"
% __status__ = "Dev"
function [value, dt_value, dtt_value]  = fun_h(t, tau, t0)
 
	s = (t-tau)/t0;
   	
    if (s<1 && s>0)
        
        f = 1.0./s + 1./(s-1);
      
		d_g = - 1/(exp(f)+2+exp(-f));
		dd_g =  d_g*(1 - 2/(1+exp(-f))) ;
		
		d_f = - 1 / ((s-1)*(s-1)) - 1/(s*s);
		dd_f = 2 / ((s-1)*(s-1)*(s-1)) + 2.0/(s*s*s);
		
        value = 1/(1+ exp(f));
        
        dt_value =   d_f*d_g/t0;
        
		dtt_value =   (dd_f*d_g+d_f*d_f*dd_g)/(t0*t0);
	
    elseif  (s<=0)
    
        value = 0;
       
        dt_value = 0;
        
        dtt_value = 0;
        
    else
        
        value = 1;
    
        dt_value = 0;
        
        dtt_value = 0;
        
    end
    
end

