% Supplementary materials of
%
% [1] Construction and convergence analysis of conservative second order 
%     local time discretisation for wave equations based on domain decomposition. 
%     Juliette Chabassier and Sebastien Imperiale
%
% __author__ = "Sebastien Imperiale"
% __copyright__ = "Copyright 2019, Inria"
% __credits__ = ["Sebastien Imperiale"]
% __license__ = "GPL 3.0"
% __version__ = "1.0"
% __maintainer__ = "Sebastien Imperiale"
% __email__ = "sebastien.imperiale@inria.fr"
% __status__ = "Dev"
function [value d_value] = fun_r(x, x0, sigma)

    value = zeros(size(x));
    d_value = zeros(size(x));
    
    indices = find(abs(x-x0)<sigma);
    xi = x(indices);
    
    numerator = 1.0-(1.0./(sigma^2)).*(xi-x0).^2;
    
    value(indices) = exp(-2.0./numerator);
    
    d_value(indices) = -exp(-2.0./numerator).* ...
                    (4.0/sigma^2).*(xi-x0)./(numerator.^2);
    
  
end

