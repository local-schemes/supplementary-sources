% Supplementary materials of
%
% [1] Construction and convergence analysis of conservative second order 
%     local time discretisation for wave equations based on domain decomposition. 
%     Juliette Chabassier and Sebastien Imperiale
%
%%
% __author__ = "Sebastien Imperiale"
% __copyright__ = "Copyright 2019, Inria"
% __credits__ = ["Sebastien Imperiale"]
% __license__ = "GPL 3.0"
% __version__ = "1.0"
% __maintainer__ = "S?bastien Imperiale"
% __email__ = "sebastien.imperiale@inria.fr"
% __status__ = "Dev"


clear all
close all

%Common parameters of the numerical experiments
Parameters.T = 0.5;

Parameters.x0 = -0.25;
Parameters.sigma = 0.05;
Parameters.tau = 0.025;
Parameters.t0 = 0.0625;

Parameters.Display = 0;  % 0 for no display, 1 to display the solutions
Parameters.NExp = 8; %Number of mesh refinement for the convergence plots

%Array storing the compute relative errors
Errors = zeros(Parameters.NExp,1);
 
%boolean for writing errors in .txt files
write_data = 1;

%% Results of Section 5.1 of [1]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Parameters.Model_type = 'propagating_pulse';
Parameters.Method = 'locally_implicit_2';

Parameters.Mu = 1;
Parameters.alpha = 1;
Parameters.qf = 10;

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,1) = SupErrorL2Relative;
    Errors(n,2) = SupErrorH1Relative;
   
end

Parameters.Mu = 2;
Parameters.alpha = 1;
Parameters.qf = 20;

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,3) = SupErrorL2Relative;
    Errors(n,4) = SupErrorH1Relative;
   
end

figure(1)
clf;
loglog(h,Errors(:,1),'bo-')
title('Convergence plot (Section 5.1 of [1]) (mu, q) = (1,10)')
xlabel('h')
ylabel('Relative L2 Error')
grid on
hold on

figure(2) 
clf;
loglog(h,Errors(:,2),'bo-')
title('Convergence plot (Section 5.1 of [1]) (mu, q) = (1,10)')
xlabel('h')
ylabel('Relative H1 Error')
grid on
hold on

 figure(3)
clf;
loglog(h,Errors(:,3),'bo-')
title('Convergence plot (Section 5.1 of [1]) (mu, q) = (2,20)')
xlabel('h')
ylabel('Relative L2 Error')
grid on
hold on

figure(4) 
clf;
loglog(h,Errors(:,4),'bo-')
title('Convergence plot (Section 5.1 of [1]) (mu, q) = (2,20)')
xlabel('h')
ylabel('Relative H1 Error')
grid on
hold on

if(write_data)
    fid= fopen(['Results/locally_implicit.txt'],'w'); 
    fprintf(fid,...
        ['h H1_Mu_1_Qf_10 H1_Mu_2_Qf_20 \n']);
    fprintf(fid,'%1.11e %1.11e %1.11e \n'...
        ,[h', Errors(:,2), Errors(:,4)]'); 
    fclose(fid);
end

%% Results of Section 5.2 of [1]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Parameters.Model_type = 'propagating_pulse';
Parameters.Method = 'lts_implicit_2';

Parameters.Mu = 1;
Parameters.qf = 2;
Parameters.alpha = 0.9;

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,1) = SupErrorL2Relative;
    Errors(n,2) = SupErrorH1Relative;
   
end

Parameters.alpha = 0.99;

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,3) = SupErrorL2Relative;
    Errors(n,4) = SupErrorH1Relative;
   
end

Parameters.alpha = 0.999;

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,5) = SupErrorL2Relative;
    Errors(n,6) = SupErrorH1Relative;
   
end

Parameters.alpha = 1;

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,7) = SupErrorL2Relative;
    Errors(n,8) = SupErrorH1Relative;
   
end



figure(5)
clf;
loglog(h,Errors(:,1),'bo-',h,Errors(:,3),...
    'ro-',h,Errors(:,5),'b+-',h,Errors(:,7),'r+-')
title('Convergence plot (Section 5.2 of [1]) (mu, q) = (1,2)')
xlabel('h')
ylabel('Relative L2 Error')
grid on
hold on

figure(6) 
clf;
loglog(h,Errors(:,2),'bo-',h,Errors(:,4),...
    'ro-',h,Errors(:,6),'b+-',h,Errors(:,8),'r+-')
title('Convergence plot (Section 5.2 of [1]) (mu, q) = (1,2)')
xlabel('h')
ylabel('Relative H1 Error')
grid on
hold on


if(write_data)
    fid= fopen(['Results/lts_implicit_Mu_1_qf_2.txt'],'w'); 
    fprintf(fid,...
        ['h H1_a_09 H1_a_099 H1_a_0999 H1_a_1 \n']);
    fprintf(fid,'%1.11e %1.11e %1.11e %1.11e %1.11e \n', ...
        [h', Errors(:,2), Errors(:,4), Errors(:,6), Errors(:,8)]'); 
    fclose(fid);
end


Parameters.Mu = 0.25;
Parameters.qf = 4;
Parameters.alpha = 0.9;


for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,1) = SupErrorL2Relative;
    Errors(n,2) = SupErrorH1Relative;
   
end

Parameters.alpha = 0.99;

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,3) = SupErrorL2Relative;
    Errors(n,4) = SupErrorH1Relative;
   
end

Parameters.alpha = 0.999;

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,5) = SupErrorL2Relative;
    Errors(n,6) = SupErrorH1Relative;
   
end

Parameters.alpha = 1;

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,7) = SupErrorL2Relative;
    Errors(n,8) = SupErrorH1Relative;
   
end

figure(7)
clf;
loglog(h,Errors(:,1),'bo-',h,Errors(:,3),...
    'ro-',h,Errors(:,5),'b+-',h,Errors(:,7),'r+-')
title('Convergence plot (Section 5.2 of [1]) (mu, q) = (1,2)')
xlabel('h')
ylabel('Relative L2 Error')
grid on
hold on

figure(8) 
clf;
loglog(h,Errors(:,2),'bo-',h,Errors(:,4),...
    'ro-',h,Errors(:,6),'b+-',h,Errors(:,8),'r+-')
title('Convergence plot (Section 5.2 of [1]) (mu, q) = (1,2)')
xlabel('h')
ylabel('Relative H1 Error')
grid on
hold on

if(write_data)
    fid= fopen(['Results/lts_implicit_Mu_025_qf_4.txt'],'w'); 
    fprintf(fid,...
        ['h H1_a_09 H1_a_099 H1_a_0999 H1_a_1 \n']);
    fprintf(fid,'%1.11e %1.11e %1.11e %1.11e %1.11e \n', ...
        [h', Errors(:,2), Errors(:,4), Errors(:,6), Errors(:,8)]'); 
    fclose(fid);
end

%% Results of Section 5.3 of [1]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Parameters.Model_type = 'propagating_pulse';

Parameters.Method = 'lts_implicit_3';
Parameters.Mu = 1;
Parameters.qf = 3;
Parameters.alpha = 0.9960;

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,1) = SupErrorL2Relative;
    Errors(n,2) = SupErrorH1Relative;
   
end

Parameters.Method = 'lts_implicit_3b';

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,3) = SupErrorL2Relative;
    Errors(n,4) = SupErrorH1Relative;
   
end

figure(1)
clf;
loglog(h,Errors(:,1),'bo-',h,Errors(:,3),'ro-')
title('Convergence plot (Section 5.3 of [1])')
xlabel('h')
ylabel('Relative L2 Error')
grid on
hold on

figure(2)
clf;
loglog(h,Errors(:,2),'bo-',h,Errors(:,4),'ro-')
title('Convergence plot (Section 5.3 of [1])')
xlabel('h')
ylabel('Relative H1 Error')
grid on
hold on

if(write_data)
    fid= fopen(['Results/lts_implicit_3.txt'],'w'); 
    fprintf(fid,...
        ['h H1_LTS3 H1_LTS3b \n']);
    fprintf(fid,'%1.11e %1.11e %1.11e \n', ...
        [h', Errors(:,2), Errors(:,4)]'); 
    fclose(fid);
end


Parameters.Method = 'lts_implicit_4';
Parameters.qf = 4;
Parameters.Mu = 1;
Parameters.alpha = 0.9960;

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,1) = SupErrorL2Relative;
    Errors(n,2) = SupErrorH1Relative;
   
end

Parameters.Method = 'lts_implicit_4b';

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,3) = SupErrorL2Relative;
    Errors(n,4) = SupErrorH1Relative;
   
end

figure(3)
clf;
loglog(h,Errors(:,1),'bo-',h,Errors(:,3),'ro-')
title('Convergence plot (Section 5.3 of [1])')
xlabel('h')
ylabel('Relative L2 Error')
grid on
hold on

figure(4)
clf;
loglog(h,Errors(:,2),'bo-',h,Errors(:,4),'ro-')
title('Convergence plot (Section 5.3 of [1])')
xlabel('h')
ylabel('Relative H1 Error')
grid on
hold on

if(write_data)
    fid= fopen(['Results/lts_implicit_4.txt'],'w'); 
    fprintf(fid,...
        ['h H1_LTS4 H1_LTS4b \n']);
    fprintf(fid,'%1.11e %1.11e %1.11e \n', ...
        [h', Errors(:,2), Errors(:,4)]'); 
    fclose(fid);
end


