% Supplementary materials of
%
% [1] Construction and convergence analysis of conservative second order 
%     local time discretisation for wave equations based on domain decomposition. 
%     Juliette Chabassier and Sebastien Imperiale
%
% __author__ = "Sebastien Imperiale"
% __copyright__ = "Copyright 2019, Inria"
% __credits__ = ["Sebastien Imperiale"]
% __license__ = "GPL 3.0"
% __version__ = "1.0"
% __maintainer__ = "Sebastien Imperiale"
% __email__ = "sebastien.imperiale@inria.fr"
% __status__ = "Dev"

function [SupErrorL2Relative SupErrorH1Relative] = solver( Parameters )

hc = 0.5/Parameters.Nc;
hf = 0.5/Parameters.Nf;
Ndofc = 2*Parameters.Nc+1;
Ndoff = 2*Parameters.Nf+1;
Ndof = 2*(Parameters.Nc+Parameters.Nf)+1;

xc = linspace(-0.5,0,Ndofc)';
xf = linspace(0,0.5,Ndoff)';
x = [xc ; xf(2:end)];

K = sparse(Ndof,Ndof);
M = zeros(Ndof,1);
P = ones(Ndof,1);

Kc = sparse(Ndofc,Ndofc);
Mc = zeros(Ndofc,1);

Kf = sparse(Ndoff,Ndoff);
Mf = zeros(Ndoff,1);

fc = zeros(Ndofc,1);
ff = zeros(Ndoff,1);
f = zeros(Ndof,1);

uc_n = zeros(Ndofc,1);
uf_n = zeros(Ndoff,1);
u_n = zeros(Ndof,1);

uc_n_1 = zeros(Ndofc,1);
uf_n_1 = zeros(Ndoff,1);
u_n_1 = zeros(Ndof,1);

uc_1_n = zeros(Ndofc,1);
uf_1_n = zeros(Ndoff,1);
u_1_n = zeros(Ndof,1);

uc_exact = zeros(Ndofc,1);
uf_exact = zeros(Ndoff,1);
u_exact = zeros(Ndof,1);


Kelem =  [7 -8 1 ; -8  16 -8 ; 1 -8 7 ]/3;
Melem =  [1 ; 4; 1]/6;


%Assemble Mc and Kc
for n=1:Parameters.Nc
    
    b = 2*(n-1)+1;
    e = 2*(n-1)+3;
    
    Mc(b:e) = Mc(b:e) + hc*Melem;
    Kc(b:e,b:e) =  Kc(b:e,b:e) +  (1/hc)*Kelem;
    
end

%Assemble Mf and Kf
for n=1:Parameters.Nf
    
    b = 2*(n-1)+1;
    e = 2*(n-1)+3;
    
    c2 = Parameters.Mu;
    
    Mf(b:e) = Mf(b:e) + hf*Melem;
    Kf(b:e,b:e) =  Kf(b:e,b:e) +  (c2/hf)*Kelem;
    
end

%construct P
if  strcmp(Parameters.Method,'lts_explicit_2') ...
        || strcmp(Parameters.Method,'lts_explicit_2b')
    P(1:2*(Parameters.Nc-Parameters.Noverlap)) = 0;
end

%construct M
M(1:Ndofc) = Mc(1:Ndofc) ;
M(Ndofc:end) = M(Ndofc:end) + Mf(1:Ndoff);

%construct K
K(1:Ndofc,1:Ndofc) = Kc(1:Ndofc,1:Ndofc) ;
K(Ndofc:end,Ndofc:end) = K(Ndofc:end,Ndofc:end) + Kf(1:Ndoff,1:Ndoff);

%Compute the optimal time step based on the coarse domain discretisation
MinvHalf = spdiags(1.0./sqrt(Mc),0,Ndofc,Ndofc);
A = MinvHalf*Kc*MinvHalf;
rho  = normest(A,1e-6);
dtopt = 2/sqrt(rho);

%Compute the actual dt and the number of time step
dt = Parameters.alpha*dtopt;
Parameters.dt=dt;
Ndt = ceil(Parameters.T/dt);

%construt the polynomial required for the various lts schemes
if strcmp(Parameters.Method,'lts_implicit_3')
    
    PolyOrder = 2;
    Poly = [1 -6/81 1/729];
    
elseif strcmp(Parameters.Method,'lts_implicit_3b')
    
    PolyOrder = 2;
    Poly = [1 ...
            -0.074143499508064402409078412994491 ...
            0.0013794524301445731391184938669347];
    
elseif strcmp(Parameters.Method,'lts_implicit_4')
    
    PolyOrder = 3;
    Poly = [1 -20/256 8/4096 -1/65536];
    
elseif strcmp(Parameters.Method,'lts_implicit_4b')
    
    PolyOrder = 3;
    Poly = [1 ...
            -0.078223896045943285018397542535095 ...
            0.0019654870309851008215373573466587 ...
            -0.000015452557142592873886490972402839];
end


%construt the matrix T_lambda associated with System (68) of [1]
if  strcmp(Parameters.Method,'lts_implicit_2')
    
    Cf = zeros(Ndoff,1);
    Cf(1) = 1;
    
    Cf =  Cf -  dt*dt*Kf*(Cf./Mf)/16;
      
    T_lambda = (1/Mc(end) + Cf(1)/Mf(1));
end

if  strcmp(Parameters.Method,'locally_implicit_2')
    
     Cf = zeros(Ndoff,1);
     Cf(1) = 1;
     Af = spdiags(Mf,0,Ndoff,Ndoff) + dt*dt*Kf/4;
     
     Bf =  Af \ Cf;
       
     T_lambda = ( 1/Mc(end) + Bf(1));
     
     
end

if strcmp(Parameters.Method,'lts_implicit_3' ) ...
|| strcmp(Parameters.Method,'lts_implicit_3b') ...
|| strcmp(Parameters.Method,'lts_implicit_4' ) ...
|| strcmp(Parameters.Method,'lts_implicit_4b') 
     
     Cf = zeros(Ndoff,1);
     Cf(1) = 1;
     
     v = Cf;
     for i = 2:(PolyOrder+1)
          v = dt*dt*Kf*(v./Mf);
          Cf =  Cf + Poly(i)*v;
     end
     
     T_lambda = (1/Mc(end) + Cf(1)/Mf(1));
end

 
 

%Preallocation of error and norm vectors
ErrorL2c = zeros(1,Ndt);  ErrorSH1c = zeros(1,Ndt); ErrorL2f = zeros(1,Ndt);
ErrorSH1f = zeros(1,Ndt); NormL2c = zeros(1,Ndt); NormSH1c = zeros(1,Ndt);
NormL2f = zeros(1,Ndt); NormSH1f = zeros(1,Ndt);

%Loop over the time iterations
for n=1:Ndt
    
    %Compute the exact solution's next iterate and source term
    if strcmp(Parameters.Model_type,'propagating_pulse')
        
        velocity = sqrt(Parameters.Mu);
        T = 2.0/(1+velocity);
        R = (1-sqrt(Parameters.Mu))/(1+velocity);
      
        [sol_r d_sol_r] ...
            = fun_r(xc-(n+1)*dt,Parameters.x0,Parameters.sigma);
        
        R_sol_r = fun_r(-xc-(n+1)*dt,Parameters.x0,Parameters.sigma);
       
        T_sol_r = fun_r(xf/velocity-(n+1)*dt,Parameters.x0,Parameters.sigma);
        
        [sol_t dt_sol_t dtt_sol_t] ...
            = fun_h((n+1)*dt,Parameters.tau,Parameters.t0);
        
        uc_exact = (sol_r + R*R_sol_r)*sol_t; 
        uf_exact = T*T_sol_r*sol_t;
        u_exact = [uc_exact ; uf_exact(2:end)];
       
        
        [sol_r d_sol_r] ...
            = fun_r(xc-n*dt,Parameters.x0,Parameters.sigma);
        
        [sol_t dt_sol_t dtt_sol_t] ...
            = fun_h(n*dt,Parameters.tau,Parameters.t0);
        
        fc = -2*d_sol_r*dt_sol_t + sol_r*dtt_sol_t;
                    
        ff = zeros(Ndoff,1);           
        
        f = [fc ; ff(2:end)];
        
    elseif strcmp(Parameters.Model_type,'static_solution')
        
        [sol_x dxx_sol_x] = fun_g(x);
        
        [sol_t dtt_sol_t] = fun_h((n+1)*dt,Parameters.tau,Parameters.t0);
        
        u_exact = sol_t*sol_x;
        uc_exact = u_exact(1:Ndofc);
        uf_exact = u_exact(Ndofc:end);
        
        [sol_t dt_sol_t dtt_sol_t] ...
            = fun_h(n*dt,Parameters.tau,Parameters.t0);
        
        f = sol_x*dtt_sol_t - dxx_sol_x*sol_t;
        
        fc = f(1:Ndofc);
        ff = f(Ndofc:end);
        
    elseif strcmp(Parameters.Model_type,'constante_solution')
        
        [sol_t dt_sol_t dtt_sol_t] ...
            = fun_h((n+1)*dt,Parameters.tau,Parameters.t0);
        
        u_exact = sol_t*ones(Ndof,1);
        uc_exact = u_exact(1:Ndofc);
        uf_exact = u_exact(Ndofc:end);
        
        [sol_t dt_sol_t dtt_sol_t] ...
            = fun_h(n*dt,Parameters.tau,Parameters.t0);
        
        f = ones(Ndof,1)*dtt_sol_t;
        
        fc = f(1:Ndofc);
        ff = f(Ndofc:end);
        
    end
    
    %Compute the approximate solution's next iterate
    if strcmp(Parameters.Method,'lts_explicit_2')
        % Eq. (EX-2) of [1]
        
        v = (K*u_n./M - f);
        u_1_n = 2*u_n - u_n_1 + dt*dt*( - v + (dt*dt/16)*(K*(P.*v))./M);
        uc_1_n = u_1_n(1:Ndofc);
        uf_1_n = u_1_n(Ndofc:end);
        
    elseif strcmp(Parameters.Method,'lts_explicit_2b')
        % Eq. (EX-2b) of [1]
        v = K*u_n./M;
        
        u_1_n = 2*u_n - u_n_1 + dt*dt*(f - v + (dt*dt/16)*(K*(P.*v))./M);
        uc_1_n = u_1_n(1:Ndofc);
        uf_1_n = u_1_n(Ndofc:end);
        
    elseif  strcmp(Parameters.Method,'locally_implicit_2') 
             
        %prediction
        uc_1_n = 2*uc_n - uc_n_1 + dt*dt*(fc - Kc*uc_n./Mc);
    
        uf_1_n = 2*uf_n - uf_n_1 + dt*dt*((Af)\(ff - Kf*uf_n));
    
        %Transmission
        lambda = (uf_1_n(1)-uc_1_n(end))/(dt*dt*T_lambda);
        uc_1_n(end)  = uc_1_n(end) + dt*dt*lambda/Mc(end);     
        uf_1_n  = uf_1_n - dt*dt*lambda*Bf;
        
        u_1_n = [uc_1_n ; uf_1_n(2:end)];
        
        
    elseif  strcmp(Parameters.Method,'lts_implicit_2') 
        % Scheme (67) of [1]          
        
        %prediction
        uc_1_n = 2*uc_n - uc_n_1 + dt*dt*(fc - Kc*uc_n./Mc);
    
        tmp = Kf*uf_n - Mf.*ff;
        tmp = tmp - dt*dt*Kf*(tmp./Mf)/16;
        
        uf_1_n = 2*uf_n - uf_n_1 - dt*dt*(tmp./Mf);
    
        %Transmission
        lambda = (uf_1_n(1)-uc_1_n(end))/(dt*dt*T_lambda);
        uc_1_n(end)  = uc_1_n(end) + dt*dt*lambda/Mc(end);
        uf_1_n  = uf_1_n - dt*dt*lambda*Cf./Mf;
    
        u_1_n = [uc_1_n ; uf_1_n(2:end)];
        
    elseif strcmp(Parameters.Method,'lts_implicit_3' ) ...
         ||strcmp(Parameters.Method,'lts_implicit_3b') ...
         ||strcmp(Parameters.Method,'lts_implicit_4' ) ...
         ||strcmp(Parameters.Method,'lts_implicit_4b')  
     
        %prediction
        uc_1_n = 2*uc_n - uc_n_1 + dt*dt*(fc - Kc*uc_n./Mc);
        
        w = Kf*uf_n - Mf.*ff;
        v = w;
        for i = 2:(PolyOrder+1)
          v = dt*dt*Kf*(v./Mf);
          w =  w + Poly(i)*v;
        end
     
        uf_1_n = 2*uf_n - uf_n_1 - dt*dt*(w./Mf);
        
        %Transmission
        lambda = (uf_1_n(1)-uc_1_n(end))/(dt*dt*T_lambda);
        uc_1_n(end)  = uc_1_n(end) + dt*dt*lambda/Mc(end);
        uf_1_n  = uf_1_n - dt*dt*lambda*Cf./Mf;
     
        u_1_n = [uc_1_n ; uf_1_n(2:end)];
   
    end
    
    
    %Compute error and norms
    vc = uc_exact-uc_1_n;
    vf = uf_exact-uf_1_n;
    ErrorL2c(n) = sqrt(vc' * (Mc .* vc));
    ErrorSH1c(n) = sqrt(vc' * Kc * vc);
    ErrorL2f(n) = sqrt(vf' * (Mf .* vf));
    ErrorSH1f(n) = sqrt(vf' * Kf * vf);
    
    vc = uc_exact;
    vf = uf_exact;
    NormL2c(n) = sqrt(vc' * (Mc .* vc));
    NormSH1c(n) = sqrt(vc' * Kc * vc);
    NormL2f(n) = sqrt(vf' * (Mf .* vf));
    NormSH1f(n) = sqrt(vf' * Kf * vf);
    
    
    %Display options
    if (Parameters.Display>0)
        figure(Parameters.Display)
        plot(x,u_exact,x,u_1_n)
        pause(0.01)
    end
    
    %Shift
    u_n_1 = u_n;
    u_n = u_1_n;
    
    uc_n_1 = uc_n;
    uc_n = uc_1_n;
    
    uf_n_1 = uf_n;
    uf_n = uf_1_n;
    
end

%Compute the supremum errors and norms
SupErrorL2 = max(sqrt(ErrorL2c.^2+ErrorL2f.^2));
SupErrorH1 = max(sqrt(ErrorL2c.^2+ErrorL2f.^2+ErrorSH1c.^2+ErrorSH1f.^2));

SupNormL2 = max(sqrt(NormL2c.^2+NormL2f.^2));
SupNormH1 = max(sqrt(NormL2c.^2+NormL2f.^2+NormSH1c.^2+NormSH1f.^2));

SupErrorL2Relative = SupErrorL2/SupNormL2;
SupErrorH1Relative = SupErrorH1/SupNormH1;

end

