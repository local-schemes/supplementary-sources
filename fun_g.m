% Supplementary materials of
%
% [1] Construction and convergence analysis of conservative second order 
%     local time discretisation for wave equations based on domain decomposition. 
%     Juliette Chabassier and Sebastien Imperiale
%
% __author__ = "Sebastien Imperiale"
% __copyright__ = "Copyright 2019, Inria"
% __credits__ = ["Sebastien Imperiale"]
% __license__ = "GPL 3.0"
% __version__ = "1.0"
% __maintainer__ = "S?bastien Imperiale"
% __email__ = "sebastien.imperiale@inria.fr"
% __status__ = "Dev"
function [value, dxx_value]  = fun_g(x)
 
    value =  ((x<0).*x.*x.*(0.5+x).*(0.5+x) ...
                    +(x>=0).*x.*x.*(0.5-x).*(0.5-x));
    
    dxx_value =  ((x<0).*(12*x.*x + 6*x +0.5) ...
                        +(x>=0).*(12*x.*x - 6*x +0.5));            
        
end

