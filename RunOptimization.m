% Supplementary materials of
%
% [1] Construction and convergence analysis of conservative second order 
%     local time discretisation for wave equations based on domain decomposition. 
%     Juliette Chabassier and Sebastien Imperiale
%
% __author__ = "Juliette Chabassier"
% __copyright__ = "Copyright 2020, Inria"
% __credits__ = ["Juliette Chabassier"]
% __license__ = "GPL 3.0"
% __version__ = "1.0"
% __maintainer__ = "Juliette Chabassier"
% __email__ = "Juliette.Chabassier@inria.fr"
% __status__ = "Dev"

[a,b,beta, bm] = Optimize_polynoms([1/3^6, -6/3^4, 1], 1); % for ell =2
fprintf('2 & %1.2f & %3.14f & %3.14f & %3.3f & %3.15f\n ',[1, b, a, beta, bm])
[a,b,beta, bm] = Optimize_polynoms([1/3^6, -6/3^4, 1], .5); % for ell =2
fprintf('2 & %1.2f & %3.14f & %3.14f & %3.3f & %3.15f\n ',[.5, b, a, beta, bm])
[a,b,beta, bm] = Optimize_polynoms([1/3^6, -6/3^4, 1], .1); % for ell =2
fprintf('2 & %1.2f & %3.14f & %3.14f & %3.3f & %3.15f\n ',[.1, b, a, beta, bm])

[a,b,beta, bm] = Optimize_polynoms([-1/4^8, 8/4^6, -20/4^4, 1], 1); % for ell=3
fprintf('3 & %1.2f & %3.14f & %3.14f & %3.3f & %3.15f\n ',[1, b, a, beta, bm])
[a,b,beta, bm] = Optimize_polynoms([-1/4^8, 8/4^6, -20/4^4, 1], .5); % for ell=3
fprintf('3 & %1.2f & %3.14f & %3.14f & %3.3f & %3.15f\n ',[.5, b, a, beta, bm])
[a,b,beta, bm] = Optimize_polynoms([-1/4^8, 8/4^6, -20/4^4, 1], .1); % for ell=3
fprintf('3 & %1.2f & %3.14f & %3.14f & %3.3f & %3.15f\n ',[.1, b, a, beta, bm])

[a,b,beta, bm] = Optimize_polynoms([1/5^10, -10/5^8, 35/5^6, -50/5^4, 1], 1); % for ell=4
fprintf('4 & %1.2f & %3.14f & %3.14f & %3.3f & %3.15f\n ',[1, b, a, beta, bm])
[a,b,beta, bm] = Optimize_polynoms([1/5^10, -10/5^8, 35/5^6, -50/5^4, 1], .5); % for ell=4
fprintf('4 & %1.2f & %3.14f & %3.14f & %3.3f & %3.15f\n ',[.5, b, a, beta, bm])
[a,b,beta, bm] = Optimize_polynoms([1/5^10, -10/5^8, 35/5^6, -50/5^4, 1], .1); % for ell=4
fprintf('4 & %1.2f & %3.14f & %3.14f & %3.3f & %3.15f\n ',[.1, b, a, beta, bm])
