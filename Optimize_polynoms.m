% Supplementary materials of
%
% [1] Construction and convergence analysis of conservative second order 
%     local time discretisation for wave equations based on domain decomposition. 
%     Juliette Chabassier and Sebastien Imperiale
%
% __author__ = "Juliette Chabassier"
% __copyright__ = "Copyright 2020, Inria"
% __credits__ = ["Juliette Chabassier"]
% __license__ = "GPL 3.0"
% __version__ = "1.0"
% __maintainer__ = "Juliette Chabassier"
% __email__ = "Juliette.Chabassier@inria.fr"
% __status__ = "Dev"

function [a_opt2,b_opt2,beta_opt2, beta_max]=Optimize_polynoms(poly,epsilon)
% [a,b,beta] = Optimize_polynoms([1/3^6, -6/3^4, 1], .1) % for ell =2
% [a,b,beta] = Optimize_polynoms([-1/4^8, 8/4^6, -20/4^4, 1], .1) % for ell=3
% [a,b,beta] = Optimize_polynoms([1/5^10, -10/5^8, 35/5^6, -50/5^4, 1], .1) % for ell=4

ell = length(poly)-1;
poly_prime = [ell:-1:0].*poly;

Bornes = [20 40 70 100];
x = 0.1:0.01:Bornes(ell);
P_tilde_p2 = polyval(poly,x);%1-6*x/3^4 + x.*x/3^6;

beta = epsilon;
alpha = 1-epsilon/4;

% intermediate polynoms
tildeQx = alpha *x.* P_tilde_p2 + beta; %(a,b,eps) = (1 0 1)

% step i)
r = roots([poly beta/alpha]);
b_opt2 = min(r); % b_epsilon
tildefx = 1*x+b_opt2;
tildeQxfx = alpha*(tildefx.*(polyval(poly, tildefx)))+beta;%(a,b,eps) = (1 b_epsilon 1)

% step ii)
a_opt2 = 1/(alpha*polyval(poly_prime+poly,b_opt2)); % a_epsilon
a_x_plus_b = a_opt2*x+b_opt2;
Qxfx = alpha*(a_x_plus_b.*(polyval(poly, a_x_plus_b)))+beta; % optimized polynom : (a,b,eps) = (a_epsilon b_epsilon 1)

% step iii) for odd cases
if(mod(ell,2)==0)
    % even
    beta_opt2 = sqrt((4*(ell+1)^2 - b_opt2)/a_opt2)/2;
    beta_max= beta_opt2;
    mimi = 4;
else
    %odd
    % numerical optim 
    beta_max = sqrt((4*(ell+1)^2 - b_opt2)/a_opt2)/2;
    xx = linspace(4*beta_max^2-10, 4*beta_max^2+10, 1e3);
    a_x_plus_b = a_opt2*xx+b_opt2;
    Qxfx2 = alpha*(a_x_plus_b.*(polyval(poly, a_x_plus_b)))+beta; % optimized polynom : (a,b,eps) = (a_epsilon b_epsilon 1)
    [mimi,indi] = min(abs(Qxfx2-epsilon));
    mimi = epsilon;
    beta_opt2 = sqrt(xx(indi))/2;    
end

% figures 
figure(1), clf
plot(x,x.*P_tilde_p2,x,tildeQx,x,tildeQxfx,x,Qxfx)
hold on, plot( 4*beta_opt2^2, mimi,'k+')
ylim([0 4])


