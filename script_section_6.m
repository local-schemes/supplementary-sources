% Supplementary materials of
%
% [1] Construction and convergence analysis of conservative second order 
%     local time discretisation for wave equations based on domain decomposition. 
%     Juliette Chabassier and Sebastien Imperiale
%
%%
% __author__ = "Sebastien Imperiale"
% __copyright__ = "Copyright 2019, Inria"
% __credits__ = ["Sebastien Imperiale"]
% __license__ = "GPL 3.0"
% __version__ = "1.0"
% __maintainer__ = "S?bastien Imperiale"
% __email__ = "sebastien.imperiale@inria.fr"
% __status__ = "Dev"


clear all
close all

%Common parameters of the numerical experiments
Parameters.Mu = 1;
Parameters.T = 0.5;
Parameters.alpha = 0.99;
Parameters.qf = 2;
Parameters.Noverlap = 1;
Parameters.x0 = -0.25;
Parameters.sigma = 0.05;


Parameters.Display = 0;  % 0 for no display, 1 to display the solutions
Parameters.NExp = 8; %Number of mesh refinement for the convergence plots

%Array storing the compute relative errors
Errors = zeros(Parameters.NExp,1);
 
%boolean for writing errors in .txt files
write_data = 1;

%% Results of Section 6.3.1 of [1]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Parameters.Model_type = 'propagating_pulse';
Parameters.tau = 0.025;
Parameters.t0 = 0.0625;

Parameters.Method = 'lts_implicit_2';


for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,1) = SupErrorL2Relative;
    Errors(n,2) = SupErrorH1Relative;
   
end

figure(1)
clf;
loglog(h,Errors(:,1),'bo-')
title('Convergence plot (Section 6.3.1 of [1])')
xlabel('h')
ylabel('Relative L2 Error')
grid on
hold on


figure(2) 
clf;
loglog(h,Errors(:,2),'bo-')
title('Convergence plot (Section 6.3.1 of [1])')
xlabel('h')
ylabel('Relative H1 Error')
grid on
hold on

Parameters.Method = 'lts_explicit_2';
Parameters.Noverlap = 1;

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,3) = SupErrorL2Relative;
    Errors(n,4) = SupErrorH1Relative;
   
end

figure(1)
loglog(h,Errors(:,3),'r+-.')

figure(2)
loglog(h,Errors(:,4),'r+-.')

Parameters.Method = 'lts_explicit_2b';

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,5) = SupErrorL2Relative;
    Errors(n,6) = SupErrorH1Relative;
   
end

figure(1)
loglog(h,Errors(:,5),'mx--')
legend('IM 2','EX 2','EX 2b')
hold off

figure(2)
loglog(h,Errors(:,6),'mx--')
legend('IM 2','EX 2','EX 2b')
hold off
 
if(write_data)
    fid= fopen(['Results/propagating_pulse.txt'],'w'); 
    fprintf(fid,['h L2IM2 H1IM2 L2EX2 H1EX2 L2EX2b H1EX2b \n']);
    fprintf(fid,'%1.11e %1.11e %1.11e %1.11e %1.11e %1.11e %1.11e \n'...
        ,[h', Errors]'); 
    fclose(fid);
end

%% Results of Section 6.3.2 of [1]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Parameters.Model_type = 'static_solution';
Parameters.tau = 0.1;
Parameters.t0 = 0.25;

Parameters.Method = 'lts_implicit_2';

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,1) = SupErrorL2Relative;
    Errors(n,2) = SupErrorH1Relative;
   
end

figure(3)
clf;
loglog(h,Errors(:,1),'bo-')
title('Convergence plot (Section 6.3.2 of [1])')
xlabel('h')
ylabel('Relative L2 Error')
grid on
hold on

figure(4) 
clf;
loglog(h,Errors(:,2),'bo-')
title('Convergence plot (Section 6.3.2 of [1])')
xlabel('h')
ylabel('Relative H1 Error')
grid on
hold on
 
Parameters.Method = 'lts_explicit_2';

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,3) = SupErrorL2Relative;
    Errors(n,4) = SupErrorH1Relative;
   

end

figure(3)
loglog(h,Errors(:,3),'r+-.')
 
figure(4)
loglog(h,Errors(:,4),'r+-.')
 
Parameters.Method = 'lts_explicit_2b';

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,5) = SupErrorL2Relative;
    Errors(n,6) = SupErrorH1Relative;
  
end

figure(3)
loglog(h,Errors(:,5),'mx--')
legend('IM 2','EX 2','EX 2b')
hold off

figure(4)
loglog(h,Errors(:,6),'mx--')
legend('IM 2','EX 2','EX 2b')
hold off

if(write_data)
    fid= fopen(['Results/quasi_static.txt'],'w'); 
    fprintf(fid,['h L2IM2 H1IM2 L2EX2 H1EX2 L2EX2b H1EX2b \n']);
    fprintf(fid,'%1.11e %1.11e %1.11e %1.11e %1.11e %1.11e %1.11e \n'...
        ,[h', Errors]'); 
    fclose(fid);
end

%% Results of Section 6.3.3 of [1]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Parameters.Model_type = 'constante_solution';
Parameters.tau = 0.1;
Parameters.t0 = 0.9;

Parameters.Method = 'lts_implicit_2';

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,1) = SupErrorL2Relative;
    Errors(n,2) = SupErrorH1Relative;
   
end

figure(5)
clf;
loglog(h,Errors(:,1),'bo-')
title('Convergence plot (Section 6.3.3 of [1])')
xlabel('h')
ylabel('Relative L2 Error')
grid on
hold on

figure(6) 
clf;
loglog(h,Errors(:,2),'bo-')
title('Convergence plot (Section 6.3.3 of [1])')
xlabel('h')
ylabel('Relative H1 Error')
grid on
hold on
 
Parameters.Method = 'lts_explicit_2';

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,3) = SupErrorL2Relative;
    Errors(n,4) = SupErrorH1Relative;
   

end

figure(5)
loglog(h,Errors(:,3),'r+-.')
 
figure(6)
loglog(h,Errors(:,4),'r+-.')
 
Parameters.Method = 'lts_explicit_2b';

for n=1:Parameters.NExp
   
    Parameters.Nc = 32*2^(n-1);
    Parameters.Nf = Parameters.Nc*Parameters.qf;
    [SupErrorL2Relative SupErrorH1Relative] = solver(Parameters);

    h(n) = 1/Parameters.Nc;
    Errors(n,5) = SupErrorL2Relative;
    Errors(n,6) = SupErrorH1Relative;
  
end

figure(5)
loglog(h,Errors(:,5),'mx--')
legend('IM 2','EX 2','EX 2b')
hold off

figure(6)
loglog(h,Errors(:,6),'mx--')
legend('IM 2','EX 2','EX 2b')
hold off

if(write_data)
    fid= fopen(['Results/constant_space.txt'],'w'); 
    fprintf(fid,['h L2IM2 H1IM2 L2EX2 H1EX2 L2EX2b H1EX2b \n']);
    fprintf(fid,'%1.11e %1.11e %1.11e %1.11e %1.11e %1.11e %1.11e \n'...
        ,[h', Errors]'); 
    fclose(fid);
end